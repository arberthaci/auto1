package al.auto1.cars.utils.extensions

import android.view.View
import androidx.core.view.isVisible

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

internal fun View.show() {
    this.let { if(!it.isVisible) it.isVisible = true }
}

internal fun View.hide() {
    this.let { if(it.isVisible) it.isVisible = false }
}