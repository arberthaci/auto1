package al.auto1.cars.utils

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

object AppConstants {

    internal const val SHARED_PREFS_NAME = "auto1_shared_prefs"

    enum class AlerterMode constructor(val type: Int) {
        ALERT_INFO(1),
        ALERT_WARNING(2),
        ALERT_SUCCESS(3),
        ALERT_FAILED(4)
    }

    enum class FragmentAnimationMode constructor(val type: Int) {
        ANIMATION_NONE(0),
        ANIMATION_FADE(1),
        ANIMATION_SLIDE(2)
    }

    enum class FilterParameter constructor(val type: Int) {
        FILTER_NOT_SPECIFIED(0),
        FILTER_MANUFACTURER(1),
        FILTER_MODEL(2),
        FILTER_YEAR(3)
    }
}