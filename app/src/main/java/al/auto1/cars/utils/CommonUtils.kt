package al.auto1.cars.utils

import al.auto1.cars.R
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.support.v4.content.res.ResourcesCompat
import android.view.View
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.kaopiz.kprogresshud.KProgressHUD
import com.tapadoo.alerter.Alerter

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

object CommonUtils {

    fun isNetworkConnected(context: Context?): Boolean = context?.let {
            val connectivityManager = it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = connectivityManager.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        } ?: false

    fun showLoadingIndicator(context: Context?): KProgressHUD {
        val loadingIndicator = KProgressHUD.create(context)
        loadingIndicator.let {
            it.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            it.setLabel(context!!.getString(R.string.global_please_wait))
            it.setCancellable(false)
            it.setDimAmount(0.25f)
            it.show()
            return it
        }
    }

    fun createAlerter(activity: Activity): Alerter? {
        val alerter = Alerter.create(activity)
        alerter?.let {
            it.setTitleTypeface(ResourcesCompat.getFont(activity, R.font.exo_semibold)!!)
            it.setTextTypeface(ResourcesCompat.getFont(activity, R.font.exo_semibold)!!)
            it.enableSwipeToDismiss()
            return it
        }
        return null
    }

    fun playYoyo(view: View?) = view?.let {
        YoYo.with(Techniques.Shake)
                .duration(700)
                .repeat(3)
                .playOn(it)
    }
}