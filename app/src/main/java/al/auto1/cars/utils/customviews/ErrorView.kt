package al.auto1.cars.utils.customviews

import al.auto1.cars.R
import al.auto1.cars.utils.extensions.hide
import al.auto1.cars.utils.extensions.show
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.error_view.view.*

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class ErrorView @JvmOverloads constructor(context: Context,
                                          attrs: AttributeSet? = null,
                                          defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.error_view, this, true)
    }

    fun setState(state: ErrorViewState) {
        when (state) {
            ErrorViewState.NO_ERROR -> {
                this.hide()
            }
            ErrorViewState.NO_CONNECTION -> {
                ll_no_results.hide()
                ll_no_response.hide()
                ll_no_connection.show()
                this.show()
            }
            ErrorViewState.NO_RESULTS -> {
                ll_no_connection.hide()
                ll_no_response.hide()
                ll_no_results.show()
                this.show()
            }
            ErrorViewState.NO_RESPONSE -> {
                ll_no_connection.hide()
                ll_no_results.hide()
                ll_no_response.show()
                this.show()
            }
        }
    }
}