package al.auto1.cars.utils.rx

import al.auto1.cars.data.network.model.WkdaResponse
import al.auto1.cars.ui.filterlist.view.FilterListMVPView
import al.auto1.cars.utils.CommonUtils
import al.auto1.cars.utils.customviews.ErrorViewState
import al.auto1.cars.utils.rx.exceptions.NoNetworkConnectionException
import al.auto1.cars.utils.rx.exceptions.NoResultsException
import android.content.Context
import io.reactivex.Observable
import io.reactivex.exceptions.Exceptions

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

object RxUtils {

    /**
     * FilterListMVPView manageExceptions
     **/
    fun manageExceptions(throwable: Throwable, view: FilterListMVPView) {
        when (throwable) {
            is NoNetworkConnectionException -> { view.showErrorView(ErrorViewState.NO_CONNECTION) }
            is NoResultsException -> { view.showErrorView(ErrorViewState.NO_RESULTS) }
            else -> { view.showErrorView(ErrorViewState.NO_RESPONSE) }
        }
    }

    fun isNetworkConnected(context: Context): Observable<Boolean> =
            Observable.just(CommonUtils.isNetworkConnected(context))

    fun checkApiResponseData(wkdaResponse: WkdaResponse?): LinkedHashMap<String, String> {
        if (wkdaResponse?.wkda?.size != 0) return wkdaResponse!!.wkda!!
        else throw Exceptions.propagate(NoResultsException())
    }
}