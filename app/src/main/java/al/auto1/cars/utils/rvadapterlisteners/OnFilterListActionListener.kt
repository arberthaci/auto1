package al.auto1.cars.utils.rvadapterlisteners

/**
 * Created by Arbër Thaçi on 19-02-19.
 * Email: arberlthaci@gmail.com
 */

interface OnFilterListActionListener {

    fun onItemSelected(position: Int)
}