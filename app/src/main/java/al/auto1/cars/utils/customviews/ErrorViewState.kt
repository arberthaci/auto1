package al.auto1.cars.utils.customviews

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

enum class ErrorViewState {
    NO_ERROR,
    NO_CONNECTION,
    NO_RESULTS,
    NO_RESPONSE
}