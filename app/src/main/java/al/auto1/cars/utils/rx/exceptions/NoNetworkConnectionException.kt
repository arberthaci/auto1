package al.auto1.cars.utils.rx.exceptions

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class NoNetworkConnectionException: RuntimeException()