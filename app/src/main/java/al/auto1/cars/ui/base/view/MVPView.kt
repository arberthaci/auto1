package al.auto1.cars.ui.base.view

import al.auto1.cars.utils.AppConstants
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface MVPView {

    fun showLoadingIndicator()

    fun dismissLoadingIndicator()

    fun showAlerter(alerterMode: AppConstants.AlerterMode, message: String)

    fun isNetworkConnected(): Observable<Boolean>

    fun hasNetworkConnection(): Boolean
}