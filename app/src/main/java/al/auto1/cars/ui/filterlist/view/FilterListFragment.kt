package al.auto1.cars.ui.filterlist.view

import al.auto1.cars.R
import al.auto1.cars.ui.base.view.BaseFragment
import al.auto1.cars.ui.filterlist.interactor.FilterListMVPInteractor
import al.auto1.cars.ui.filterlist.presenter.FilterListMVPPresenter
import al.auto1.cars.utils.AppConstants
import al.auto1.cars.utils.customviews.ErrorViewState
import al.auto1.cars.utils.extensions.hide
import al.auto1.cars.utils.extensions.show
import al.auto1.cars.utils.rvadapterlisteners.OnFilterListActionListener
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_filter_list.*
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class FilterListFragment : BaseFragment(), FilterListMVPView, OnFilterListActionListener {

    @Inject
    internal lateinit var presenter: FilterListMVPPresenter<FilterListMVPView, FilterListMVPInteractor>
    @Inject
    internal lateinit var filterListAdapter: FilterListAdapter
    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    var filterParameter = AppConstants.FilterParameter.FILTER_NOT_SPECIFIED

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_filter_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }

    /**
     **********************************
     * BaseFragment implementation
     * Start
     **********************************
     **/

    override fun setUp() {
        swipe_to_refresh?.setColorSchemeResources(R.color.colorPrimary)
        swipe_to_refresh?.setOnRefreshListener { presenter.onSwipeToRefreshOccurred() }

        et_filter_list_search?.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) { presenter.afterTextChanged(input = s.toString()) }
        })
        iv_filter_list_clear_search?.setOnClickListener { presenter.onClearSearchClicked() }

        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv_filter_list?.layoutManager = layoutManager
        rv_filter_list?.itemAnimator = DefaultItemAnimator()
        rv_filter_list?.setHasFixedSize(false)
        rv_filter_list?.adapter = filterListAdapter
        filterListAdapter.onFilterListActionListener = this

        presenter.onViewPrepared(filterParameter = filterParameter)
    }

    /**
     **********************************
     * BaseFragment implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * FilterListMVPView implementation
     * Start
     **********************************
     **/

    override fun prepareLoading() {
        showLoadingIndicator()
        ev_filter_list?.setState(ErrorViewState.NO_ERROR)
        rv_filter_list?.hide()
        filterListAdapter.presenter.clearList()
    }

    override fun displayFilterList(values: List<String>?): Unit? = values?.let {
        filterListAdapter.presenter.insertFiltersToList(it)
        ev_filter_list?.setState(ErrorViewState.NO_ERROR)
        rv_filter_list?.show()
        swipe_to_refresh?.isRefreshing = false
        dismissLoadingIndicator()
    }

    override fun showErrorView(state: ErrorViewState) {
        rv_filter_list?.hide()
        ev_filter_list?.setState(state)
        swipe_to_refresh?.isRefreshing = false
        dismissLoadingIndicator()
    }

    override fun updateSearchPlaceholder(filterPlaceholder: String) {
        et_filter_list_search?.hint = getString(R.string.filter_list_search_by_name_hint, filterPlaceholder)
    }

    override fun idleSearchIcon() {
        iv_filter_list_search?.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_search_grey_700_24dp))
    }

    override fun activateSearchIcon() {
        iv_filter_list_search?.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_search_primary_24dp))
    }

    override fun hideClearIcon() {
        iv_filter_list_clear_search?.hide()
    }

    override fun showClearIcon() {
        iv_filter_list_clear_search?.show()
    }

    override fun clearSearchInput() {
        et_filter_list_search?.text?.clear()
    }

    override fun showInvalidChoiceErrorMessage() {
        showAlerter(AppConstants.AlerterMode.ALERT_FAILED, getString(R.string.filter_list_invalid_choice_message))
    }

    override fun detachFilterListFragment(): Unit? =
            getBaseActivity()?.onBackPressed()

    /**
     **********************************
     * FilterListMVPView implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * OnFilterListActionListener implementation
     * Start
     **********************************
     **/

    override fun onItemSelected(position: Int) =
            presenter.onItemSelected(position = position)

    /**
     **********************************
     * OnFilterListActionListener implementation
     * End
     **********************************
     **/

    companion object {
        internal val TAG = "filter_list_ft"
        internal val TITLE = "Choose one"

        fun newInstance(): FilterListFragment {
            return FilterListFragment()
        }
    }
}
