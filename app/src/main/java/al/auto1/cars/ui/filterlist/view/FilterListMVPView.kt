package al.auto1.cars.ui.filterlist.view

import al.auto1.cars.ui.base.view.MVPView
import al.auto1.cars.utils.customviews.ErrorViewState

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface FilterListMVPView : MVPView {

    fun prepareLoading()
    fun displayFilterList(values: List<String>?): Unit?
    fun showErrorView(state: ErrorViewState)

    fun updateSearchPlaceholder(filterPlaceholder: String)
    fun idleSearchIcon()
    fun activateSearchIcon()
    fun hideClearIcon()
    fun showClearIcon()
    fun clearSearchInput()

    fun showInvalidChoiceErrorMessage()

    fun detachFilterListFragment(): Unit?
}