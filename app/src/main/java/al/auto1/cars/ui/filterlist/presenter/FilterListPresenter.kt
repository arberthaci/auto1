package al.auto1.cars.ui.filterlist.presenter

import al.auto1.cars.ui.base.presenter.BasePresenter
import al.auto1.cars.ui.filterlist.interactor.FilterListMVPInteractor
import al.auto1.cars.ui.filterlist.view.FilterListMVPView
import al.auto1.cars.utils.AppConstants
import al.auto1.cars.utils.customviews.ErrorViewState
import al.auto1.cars.utils.rx.RetryAfterNoNetworkConnectionWithDelay
import al.auto1.cars.utils.rx.RxUtils
import al.auto1.cars.utils.rx.SchedulerProvider
import al.auto1.cars.utils.rx.exceptions.NoNetworkConnectionException
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.exceptions.Exceptions
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class FilterListPresenter <V : FilterListMVPView, I : FilterListMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), FilterListMVPPresenter<V, I> {

    private var filterParameter = AppConstants.FilterParameter.FILTER_NOT_SPECIFIED
    private var allFilterValues: LinkedHashMap<String, String> = linkedMapOf()
    private var filteredValues: LinkedHashMap<String, String> = linkedMapOf()

    override fun onViewPrepared(filterParameter: AppConstants.FilterParameter) {
        this.filterParameter = filterParameter
        displaySearchPlaceholder()

        // delay retrieving data from server to get a better fragment animation
        compositeDisposable.add(Observable.timer(500, TimeUnit.MILLISECONDS)
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe { retrieveFiltersFromServer() }
        )
    }

    override fun onSwipeToRefreshOccurred() =
            retrieveFiltersFromServer()

    override fun afterTextChanged(input: String) {
        if (input.isEmpty()) {
            getView()?.let {
                it.idleSearchIcon()
                it.hideClearIcon()

                filteredValues.clear()
                filteredValues.putAll(allFilterValues)
                it.displayFilterList(allFilterValues.values.toList())
            }
        }
        else {
            getView()?.let {
                it.activateSearchIcon()
                it.showClearIcon()
            }
            filterListByName(keyword = input)
        }
    }

    override fun onClearSearchClicked(): Unit? =
            getView()?.clearSearchInput()

    override fun onItemSelected(position: Int) {
        val filterKey = filteredValues.keys.toList()[position]
        val filterValue = filteredValues[filterKey]
        saveFilterParameterInSharedPref(filterKey = filterKey, filterValue = filterValue ?: "Error")

        getView()?.detachFilterListFragment()
    }

    private fun displaySearchPlaceholder() {
        val filterPlaceholder = when (filterParameter) {
            AppConstants.FilterParameter.FILTER_NOT_SPECIFIED -> "nothing"
            AppConstants.FilterParameter.FILTER_MANUFACTURER -> "manufacturer"
            AppConstants.FilterParameter.FILTER_MODEL -> "model"
            AppConstants.FilterParameter.FILTER_YEAR -> "year"
        }

        getView()?.updateSearchPlaceholder(filterPlaceholder = filterPlaceholder)
    }

    private fun retrieveFiltersFromServer() {
        when (filterParameter) {
            AppConstants.FilterParameter.FILTER_NOT_SPECIFIED -> getView()?.showErrorView(ErrorViewState.NO_RESPONSE)
            AppConstants.FilterParameter.FILTER_MANUFACTURER -> retrieveManufacturers()
            AppConstants.FilterParameter.FILTER_MODEL -> retrieveModels()
            AppConstants.FilterParameter.FILTER_YEAR -> retrieveYears()
        }
    }

    private fun retrieveManufacturers() {
        getView()?.let {
            it.prepareLoading()

            compositeDisposable.add(it.isNetworkConnected()
                    .switchMap { isConnected ->
                        if (isConnected) {
                            interactor!!.callRetrieveManufacturers()
                                    .map { wkdaResponse -> RxUtils.checkApiResponseData(wkdaResponse) }
                        }
                        else throw Exceptions.propagate(NoNetworkConnectionException())
                    }
                    .retryWhen(RetryAfterNoNetworkConnectionWithDelay(3, 2))
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe (
                            { wkda -> onSuccessfulDataRetrieved(valuesList = wkda) },
                            { err -> RxUtils.manageExceptions(err, it) }
                    )
            )
        }
    }

    private fun retrieveModels() {
        val filters: HashMap<String, String> = hashMapOf()
        interactor?.retrieveManufacturerKeyFromSharedPref()?.let { filters["manufacturer"] = it }

        getView()?.let {
            it.prepareLoading()

            compositeDisposable.add(it.isNetworkConnected()
                    .switchMap { isConnected ->
                        if (isConnected) {
                            interactor!!.callRetrieveMainTypes(filterParameters = filters)
                                    .map { wkdaResponse -> RxUtils.checkApiResponseData(wkdaResponse) }
                        }
                        else throw Exceptions.propagate(NoNetworkConnectionException())
                    }
                    .retryWhen(RetryAfterNoNetworkConnectionWithDelay(3, 2))
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe (
                            { wkda ->
                                onSuccessfulDataRetrieved(valuesList = wkda)
                            },
                            { err -> RxUtils.manageExceptions(err, it) }
                    )
            )
        }
    }

    private fun retrieveYears() {
        val filters: HashMap<String, String> = hashMapOf()
        interactor?.retrieveManufacturerKeyFromSharedPref()?.let { filters["manufacturer"] = it }
        interactor?.retrieveModelKeyFromSharedPref()?.let { filters["main-type"] = it }

        getView()?.let {
            it.prepareLoading()

            compositeDisposable.add(it.isNetworkConnected()
                    .switchMap { isConnected ->
                        if (isConnected) {
                            interactor!!.callRetrieveBuiltDates(filterParameters = filters)
                                    .map { wkdaResponse -> RxUtils.checkApiResponseData(wkdaResponse) }
                        }
                        else throw Exceptions.propagate(NoNetworkConnectionException())
                    }
                    .retryWhen(RetryAfterNoNetworkConnectionWithDelay(3, 2))
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe (
                            { wkda -> onSuccessfulDataRetrieved(valuesList = wkda) },
                            { err -> RxUtils.manageExceptions(err, it) }
                    )
            )
        }
    }

    private fun onSuccessfulDataRetrieved(valuesList: LinkedHashMap<String, String>) {
        allFilterValues.clear()
        allFilterValues.putAll(valuesList)

        filteredValues.clear()
        filteredValues.putAll(allFilterValues)

        getView()?.displayFilterList(filteredValues.values.toList())
    }

    private fun filterListByName(keyword: String) {
        filteredValues.clear()

        allFilterValues.entries.forEach {
            if (it.value.contains(keyword, ignoreCase = true))
                filteredValues[it.key] = it.value
        }

        getView()?.displayFilterList(filteredValues.values.toList())
    }

    private fun saveFilterParameterInSharedPref(filterKey: String, filterValue: String) {
        interactor?.let {
            when (filterParameter) {
                AppConstants.FilterParameter.FILTER_NOT_SPECIFIED -> { getView()?.showInvalidChoiceErrorMessage() }
                AppConstants.FilterParameter.FILTER_MANUFACTURER -> {
                    it.updateManufacturerKeyInSharedPref(manufacturerKey = filterKey)
                    it.updateManufacturerValueInSharedPref(manufacturerValue = filterValue)

                    it.updateModelKeyInSharedPref(modelKey = null)
                    it.updateYearKeyInSharedPref(yearKey = null)
                }
                AppConstants.FilterParameter.FILTER_MODEL -> {
                    it.updateModelKeyInSharedPref(modelKey = filterKey)
                    it.updateModelValueInSharedPref(modelValue = filterValue)

                    it.updateYearKeyInSharedPref(yearKey = null)
                }
                AppConstants.FilterParameter.FILTER_YEAR -> {
                    it.updateYearKeyInSharedPref(yearKey = filterKey)
                    it.updateYearValueInSharedPref(yearValue = filterValue)
                }
            }
        }
    }
}