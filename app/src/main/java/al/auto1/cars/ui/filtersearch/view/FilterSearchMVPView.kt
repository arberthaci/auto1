package al.auto1.cars.ui.filtersearch.view

import al.auto1.cars.ui.base.view.MVPView
import al.auto1.cars.utils.AppConstants

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface FilterSearchMVPView : MVPView {

    fun displayNewSearchView()
    fun displaySearchResultView()

    fun resetDisplayedData()
    fun displayManufacturerValue(value: String?)
    fun displayModelValue(value: String?)
    fun displayYearValue(value: String?)

    fun animateManufacturerView()
    fun animateModelView()
    fun animateYearView()

    fun openFilterListFragment(filterParameter: AppConstants.FilterParameter)
}