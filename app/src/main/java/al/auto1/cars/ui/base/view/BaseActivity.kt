package al.auto1.cars.ui.base.view

import al.auto1.cars.R
import al.auto1.cars.utils.AppConstants
import al.auto1.cars.utils.CommonUtils
import al.auto1.cars.utils.rx.RxUtils
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kaopiz.kprogresshud.KProgressHUD
import dagger.android.AndroidInjection
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

abstract class BaseActivity : AppCompatActivity(), MVPView, BaseFragment.CallBack {

    private var loadingIndicator: KProgressHUD? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        performDI()
        super.onCreate(savedInstanceState)
    }

    override fun showLoadingIndicator() {
        dismissLoadingIndicator()
        loadingIndicator = CommonUtils.showLoadingIndicator(this)
    }

    override fun dismissLoadingIndicator() {
        loadingIndicator?.let { if (it.isShowing) it.dismiss() }
    }

    override fun showAlerter(alerterMode: AppConstants.AlerterMode, message: String) {
        val alerter = CommonUtils.createAlerter(this)
        alerter?.let {
            when (alerterMode) {
                AppConstants.AlerterMode.ALERT_INFO -> {
                    it.setTitle(R.string.alerter_info_title)
                    it.setBackgroundColorRes(R.color.colorPrimary)
                    it.setIcon(R.drawable.ic_info_outline_white_24dp)
                }
                AppConstants.AlerterMode.ALERT_WARNING -> {
                    it.setTitle(R.string.alerter_warning_title)
                    it.setBackgroundColorRes(R.color.amber_A700)
                    it.setIcon(R.drawable.ic_warning_white_24dp)
                }
                AppConstants.AlerterMode.ALERT_SUCCESS -> {
                    it.setTitle(R.string.alerter_success_title)
                    it.setBackgroundColorRes(R.color.green_700)
                    it.setIcon(R.drawable.ic_check_circle_white_24dp)
                }
                AppConstants.AlerterMode.ALERT_FAILED -> {
                    it.setTitle(R.string.alerter_failed_title)
                    it.setBackgroundColorRes(R.color.red_A700)
                    it.setIcon(R.drawable.ic_sms_failed_white_24dp)
                }
            }
            it.setText(message)
            it.show()
        }
    }

    override fun isNetworkConnected(): Observable<Boolean> {
        return RxUtils.isNetworkConnected(applicationContext)
    }

    override fun hasNetworkConnection(): Boolean {
        return CommonUtils.isNetworkConnected(applicationContext)
    }

    private fun performDI() = AndroidInjection.inject(this)

    interface FilterSearchFragmentCallBack {
        fun onUpdateData()
        fun onResetFiltersClicked()
    }
}