package al.auto1.cars.ui.home.presenter

import al.auto1.cars.ui.base.presenter.MVPPresenter
import al.auto1.cars.ui.home.interactor.HomeMVPInteractor
import al.auto1.cars.ui.home.view.HomeMVPView
import android.view.Menu

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPPresenter<V : HomeMVPView, I : HomeMVPInteractor> : MVPPresenter<V, I> {

    fun onFragmentResumed()

    fun onBackPressed(): Boolean?

    fun onInvalidateOptionsMenu(menu: Menu?)
    fun onOptionsItemResetFiltersSelected()
}