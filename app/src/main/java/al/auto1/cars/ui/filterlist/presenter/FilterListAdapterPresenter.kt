package al.auto1.cars.ui.filterlist.presenter

import al.auto1.cars.ui.filterlist.view.FilterListAdapterView
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-19.
 * Email: arberlthaci@gmail.com
 */

class FilterListAdapterPresenter @Inject internal constructor(private val filterListItems: MutableList<String>) {

    var view: FilterListAdapterView? = null

    fun getItemCount() =
            this.filterListItems.size

    fun getItemAt(position: Int) =
            this.filterListItems[position]

    fun insertFiltersToList(values: List<String>) {
        clearList()
        loadMoreFiltersToList(values)
    }

    fun loadMoreFiltersToList(values: List<String>) {
        this.filterListItems.addAll(values)
        notifyDataSetChanged()
    }

    fun clearList() {
        this.filterListItems.clear()
        notifyDataSetChanged()
    }

    fun notifyDataSetChanged() =
            view?.adapterNotifyDataSetChanged()

    fun itemClickedAtPosition(position: Int) =
            view?.adapterItemClicked(position)
}