package al.auto1.cars.ui.base.presenter

import al.auto1.cars.ui.base.interactor.MVPInteractor
import al.auto1.cars.ui.base.view.MVPView

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?
}