package al.auto1.cars.ui.base.interactor

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface MVPInteractor {

    fun retrieveManufacturerKeyFromSharedPref(): String?
    fun retrieveModelKeyFromSharedPref(): String?
}