package al.auto1.cars.ui.filtersearch.interactor

import al.auto1.cars.ui.base.interactor.MVPInteractor

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface FilterSearchMVPInteractor : MVPInteractor {

    fun retrieveYearKeyFromSharedPref(): String?

    fun retrieveManufacturerValueFromSharedPref(): String?
    fun retrieveModelValueFromSharedPref(): String?
    fun retrieveYearValueFromSharedPref(): String?

    fun deleteAllDataInSharedPref()
}