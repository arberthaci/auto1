package al.auto1.cars.ui.home.view

import al.auto1.cars.ui.base.view.MVPView
import android.support.v4.app.Fragment
import android.view.Menu

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPView : MVPView {

    fun setToolbarTitle(title: String)

    fun getCurrentFragment(): Fragment?
    fun openFilterSearchFragment()
    fun closeFilterListFragment()

    fun invalidateOptionsMenuEvent(): Unit?
    fun showToolbarForFilterSearch(menu: Menu?): Unit?

    fun showBackButton()
    fun hideBackButton()
}