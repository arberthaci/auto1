package al.auto1.cars.ui.home.view

import al.auto1.cars.R
import al.auto1.cars.ui.base.view.BaseActivity
import al.auto1.cars.ui.filterlist.view.FilterListFragment
import al.auto1.cars.ui.filtersearch.view.FilterSearchFragment
import al.auto1.cars.ui.home.interactor.HomeMVPInteractor
import al.auto1.cars.ui.home.presenter.HomeMVPPresenter
import al.auto1.cars.utils.AppConstants
import al.auto1.cars.utils.extensions.addFragment
import al.auto1.cars.utils.extensions.getCurrentFragment
import al.auto1.cars.utils.extensions.removeFragment
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuItem
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class HomeActivity : BaseActivity(), HomeMVPView, HasSupportFragmentInjector {

    @Inject
    internal lateinit var presenter: HomeMVPPresenter<HomeMVPView, HomeMVPInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun onBackPressed() {
        presenter.onBackPressed()?.let {
            if (it) super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        presenter.onBackPressed()?.let {
            if (it) super.onBackPressed()
        }

        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)
        hideOptionsMenu(menu)
        presenter.onInvalidateOptionsMenu(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_reset_filters -> { presenter.onOptionsItemResetFiltersSelected() }
        }

        return super.onOptionsItemSelected(item)
    }

    /**
     **********************************
     * HomeMVPView implementation
     * Start
     **********************************
     **/

    override fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun getCurrentFragment(): Fragment? = supportFragmentManager?.getCurrentFragment(R.id.home_fragment_container)

    override fun openFilterSearchFragment() = openNewFragment(fragment = FilterSearchFragment.newInstance(),
            tag = FilterSearchFragment.TAG,
            title = FilterSearchFragment.TITLE,
            fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_FADE)

    override fun closeFilterListFragment() = onFragmentDetached(tag = FilterListFragment.TAG, fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_SLIDE)

    override fun invalidateOptionsMenuEvent(): Unit? = invalidateOptionsMenu()

    override fun showToolbarForFilterSearch(menu: Menu?) = menu?.let { mMenu ->
        mMenu.findItem(R.id.action_reset_filters)?.let { it.isVisible = true }
    }

    override fun showBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun hideBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
    }

    /**
     **********************************
     * HomeMVPView implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * BaseFragment.CallBack implementation
     * Start
     **********************************
     **/

    override fun onFragmentAttached() {}

    override fun onFragmentResumed() = presenter.onFragmentResumed()

    override fun onFragmentDetached(tag: String, fragmentAnimationMode: AppConstants.FragmentAnimationMode, detachInBackground: Boolean) {
        supportFragmentManager?.removeFragment(tag = tag, fragmentAnimationMode = fragmentAnimationMode)
        if (!detachInBackground) onFragmentResumed()
    }

    override fun openNewFragment(fragment: Fragment, tag: String, title: String?, fragmentAnimationMode: AppConstants.FragmentAnimationMode) {
        supportFragmentManager.addFragment(containerViewId = R.id.home_fragment_container,
                fragment = fragment,
                tag = tag,
                fragmentAnimationMode = fragmentAnimationMode)
        title?.let { setToolbarTitle(it) }
    }

    /**
     **********************************
     * BaseFragment.CallBack implementation
     * End
     **********************************
     **/

    private fun hideOptionsMenu(menu: Menu?) {
        menu?.let {
            it.findItem(R.id.action_reset_filters)?.let { mMenu -> mMenu.isVisible = false }
        }
    }
}
