package al.auto1.cars.ui.filterlist.view

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface FilterListViewHolderView {

    fun setValue(value: String? = null)
    fun setBackgroundColor()
    fun setOnClickListeners()
}