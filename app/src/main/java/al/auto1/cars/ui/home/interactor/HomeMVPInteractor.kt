package al.auto1.cars.ui.home.interactor

import al.auto1.cars.ui.base.interactor.MVPInteractor

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPInteractor : MVPInteractor