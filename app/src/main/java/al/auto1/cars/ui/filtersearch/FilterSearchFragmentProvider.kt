package al.auto1.cars.ui.filtersearch

import al.auto1.cars.ui.filtersearch.view.FilterSearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

@Module
internal abstract class FilterSearchFragmentProvider {

    @ContributesAndroidInjector(modules = [FilterSearchFragmentModule::class])
    internal abstract fun provideFilterSearchFragmentFactory(): FilterSearchFragment
}