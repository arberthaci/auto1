package al.auto1.cars.ui.filterlist.presenter

import al.auto1.cars.ui.filterlist.view.FilterListViewHolderView

/**
 * Created by Arbër Thaçi on 19-02-19.
 * Email: arberlthaci@gmail.com
 */

class FilterListViewHolderPresenter (private var filterListAdapterPresenter: FilterListAdapterPresenter) {

    fun clearViewHolder(viewHolderView: FilterListViewHolderView) = viewHolderView.let {
        it.setValue()
    }

    fun onBindViewHolder(position: Int, viewHolderView: FilterListViewHolderView) = viewHolderView.let {
        it.setValue(value = filterListAdapterPresenter.getItemAt(position))
        it.setBackgroundColor()
        it.setOnClickListeners()
    }

    fun onClickListenerItem(position: Int) = filterListAdapterPresenter.itemClickedAtPosition(position)
}