package al.auto1.cars.ui.filtersearch.presenter

import al.auto1.cars.ui.base.presenter.MVPPresenter
import al.auto1.cars.ui.filtersearch.interactor.FilterSearchMVPInteractor
import al.auto1.cars.ui.filtersearch.view.FilterSearchMVPView

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface FilterSearchMVPPresenter <V : FilterSearchMVPView, I : FilterSearchMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()

    fun onChooseManufacturerClicked(): Unit?
    fun onChooseModelClicked(): Unit?
    fun onChooseYearClicked(): Unit?

    fun onSearchClicked()
    fun onNewSearchClicked()

    fun onUpdateData()

    fun onResetFilters()
}