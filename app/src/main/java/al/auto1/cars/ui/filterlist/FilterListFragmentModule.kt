package al.auto1.cars.ui.filterlist

import al.auto1.cars.ui.filterlist.interactor.FilterListInteractor
import al.auto1.cars.ui.filterlist.interactor.FilterListMVPInteractor
import al.auto1.cars.ui.filterlist.presenter.FilterListAdapterPresenter
import al.auto1.cars.ui.filterlist.presenter.FilterListMVPPresenter
import al.auto1.cars.ui.filterlist.presenter.FilterListPresenter
import al.auto1.cars.ui.filterlist.view.FilterListAdapter
import al.auto1.cars.ui.filterlist.view.FilterListFragment
import al.auto1.cars.ui.filterlist.view.FilterListMVPView
import android.support.v7.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

@Module
class FilterListFragmentModule {

    @Provides
    internal fun provideFilterListInteractor(interactor: FilterListInteractor): FilterListMVPInteractor = interactor

    @Provides
    internal fun provideFilterListPresenter(presenter: FilterListPresenter<FilterListMVPView, FilterListMVPInteractor>)
            : FilterListMVPPresenter<FilterListMVPView, FilterListMVPInteractor> = presenter

    @Provides
    internal fun provideFilterListAdapterPresenter(): FilterListAdapterPresenter = FilterListAdapterPresenter(arrayListOf())

    @Provides
    internal fun provideFilterListAdapter(presenter: FilterListAdapterPresenter): FilterListAdapter = FilterListAdapter(presenter)

    @Provides
    internal fun provideLinearLayoutManager(fragment: FilterListFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)
}