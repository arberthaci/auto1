package al.auto1.cars.ui.filterlist.presenter

import al.auto1.cars.ui.base.presenter.MVPPresenter
import al.auto1.cars.ui.filterlist.interactor.FilterListMVPInteractor
import al.auto1.cars.ui.filterlist.view.FilterListMVPView
import al.auto1.cars.utils.AppConstants

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface FilterListMVPPresenter <V : FilterListMVPView, I : FilterListMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared(filterParameter: AppConstants.FilterParameter)

    fun onSwipeToRefreshOccurred()

    fun afterTextChanged(input: String)
    fun onClearSearchClicked(): Unit?

    fun onItemSelected(position: Int)
}