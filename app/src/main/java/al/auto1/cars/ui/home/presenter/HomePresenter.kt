package al.auto1.cars.ui.home.presenter

import al.auto1.cars.ui.base.presenter.BasePresenter
import al.auto1.cars.ui.filterlist.view.FilterListFragment
import al.auto1.cars.ui.filtersearch.view.FilterSearchFragment
import al.auto1.cars.ui.home.interactor.HomeMVPInteractor
import al.auto1.cars.ui.home.view.HomeMVPView
import al.auto1.cars.utils.rx.SchedulerProvider
import android.support.v4.app.Fragment
import android.view.Menu
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class HomePresenter<V : HomeMVPView, I : HomeMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), HomeMVPPresenter<V, I> {

    override fun onAttach(view: V?) {
        super.onAttach(view)

        getView()?.openFilterSearchFragment()
    }

    override fun onFragmentResumed() {
        when(getCurrentFragment()) {
            is FilterSearchFragment -> getView()?.let {
                it.hideBackButton()
                it.setToolbarTitle(FilterSearchFragment.TITLE)
            }
            is FilterListFragment -> getView()?.let {
                it.showBackButton()
                it.setToolbarTitle(FilterListFragment.TITLE)
            }
        }

        getView()?.invalidateOptionsMenuEvent()
    }

    override fun onBackPressed(): Boolean? {
        val currentFragment = getCurrentFragment()
        when (currentFragment) {
            is FilterSearchFragment -> return true
            is FilterListFragment -> {
                getView()?.closeFilterListFragment()
                (getCurrentFragment() as? FilterSearchFragment)?.onUpdateData()
            }
        }
        return null
    }

    override fun onInvalidateOptionsMenu(menu: Menu?) {
        when(getCurrentFragment()) {
            is FilterSearchFragment -> getView()?.showToolbarForFilterSearch(menu)
            is FilterListFragment -> { /* do nothing */ }
        }
    }

    override fun onOptionsItemResetFiltersSelected() {
        (getCurrentFragment() as? FilterSearchFragment)?.onResetFiltersClicked()
    }

    private fun getCurrentFragment(): Fragment? = getView()?.getCurrentFragment()
}