package al.auto1.cars.ui.filtersearch

import al.auto1.cars.ui.filtersearch.interactor.FilterSearchInteractor
import al.auto1.cars.ui.filtersearch.interactor.FilterSearchMVPInteractor
import al.auto1.cars.ui.filtersearch.presenter.FilterSearchMVPPresenter
import al.auto1.cars.ui.filtersearch.presenter.FilterSearchPresenter
import al.auto1.cars.ui.filtersearch.view.FilterSearchMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

@Module
class FilterSearchFragmentModule {

    @Provides
    internal fun provideFilterSearchInteractor(interactor: FilterSearchInteractor): FilterSearchMVPInteractor = interactor

    @Provides
    internal fun provideFilterSearchPresenter(presenter: FilterSearchPresenter<FilterSearchMVPView, FilterSearchMVPInteractor>)
            : FilterSearchMVPPresenter<FilterSearchMVPView, FilterSearchMVPInteractor> = presenter
}