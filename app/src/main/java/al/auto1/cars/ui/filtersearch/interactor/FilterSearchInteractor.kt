package al.auto1.cars.ui.filtersearch.interactor

import al.auto1.cars.data.db.DbOperations
import al.auto1.cars.data.network.ApiHelper
import al.auto1.cars.data.prefs.PreferenceHelper
import al.auto1.cars.ui.base.interactor.BaseInteractor
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class FilterSearchInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper, dbOperations = dbOperations), FilterSearchMVPInteractor {

    override fun retrieveYearKeyFromSharedPref(): String? =
            preferenceHelper.getYearKey()

    override fun retrieveManufacturerValueFromSharedPref(): String? =
            preferenceHelper.getManufacturerValue()

    override fun retrieveModelValueFromSharedPref(): String? =
            preferenceHelper.getModelValue()

    override fun retrieveYearValueFromSharedPref(): String? =
            preferenceHelper.getYearValue()

    override fun deleteAllDataInSharedPref() {
        preferenceHelper.let {
            it.setManufacturerKey(null)
            it.setManufacturerValue(null)
            it.setModelKey(null)
            it.setModelValue(null)
            it.setYearKey(null)
            it.setYearValue(null)
        }
    }
}