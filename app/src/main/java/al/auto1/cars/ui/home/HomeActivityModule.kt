package al.auto1.cars.ui.home

import al.auto1.cars.ui.home.interactor.HomeInteractor
import al.auto1.cars.ui.home.interactor.HomeMVPInteractor
import al.auto1.cars.ui.home.presenter.HomeMVPPresenter
import al.auto1.cars.ui.home.presenter.HomePresenter
import al.auto1.cars.ui.home.view.HomeMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

@Module
class HomeActivityModule {

    @Provides
    internal fun provideHomeInteractor(homeInteractor: HomeInteractor): HomeMVPInteractor = homeInteractor

    @Provides
    internal fun provideHomePresenter(homePresenter: HomePresenter<HomeMVPView, HomeMVPInteractor>)
            : HomeMVPPresenter<HomeMVPView, HomeMVPInteractor> = homePresenter
}