package al.auto1.cars.ui.filtersearch.view

import al.auto1.cars.R
import al.auto1.cars.ui.base.view.BaseActivity
import al.auto1.cars.ui.base.view.BaseFragment
import al.auto1.cars.ui.filterlist.view.FilterListFragment
import al.auto1.cars.ui.filtersearch.interactor.FilterSearchMVPInteractor
import al.auto1.cars.ui.filtersearch.presenter.FilterSearchMVPPresenter
import al.auto1.cars.utils.AppConstants
import al.auto1.cars.utils.CommonUtils
import al.auto1.cars.utils.extensions.hide
import al.auto1.cars.utils.extensions.show
import android.os.Bundle
import android.support.v4.widget.TextViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_filter_search.*
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class FilterSearchFragment : BaseFragment(), FilterSearchMVPView, BaseActivity.FilterSearchFragmentCallBack {

    @Inject
    internal lateinit var presenter: FilterSearchMVPPresenter<FilterSearchMVPView, FilterSearchMVPInteractor>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_filter_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }

    /**
     **********************************
     * BaseFragment implementation
     * Start
     **********************************
     **/

    override fun setUp() {
        rl_filter_search_manufacturer_btn?.setOnClickListener { presenter.onChooseManufacturerClicked() }
        rl_filter_search_model_btn?.setOnClickListener { presenter.onChooseModelClicked() }
        rl_filter_search_year_btn?.setOnClickListener { presenter.onChooseYearClicked() }

        b_search?.setOnClickListener { presenter.onSearchClicked() }
        b_new_search?.setOnClickListener { presenter.onNewSearchClicked() }

        presenter.onViewPrepared()
    }

    /**
     **********************************
     * BaseFragment implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * FilterSearchMVPView implementation
     * Start
     **********************************
     **/

    override fun resetDisplayedData() {
        tv_filter_search_manufacturer_label?.text = getString(R.string.filter_search_choose_manufacturer_label)
        tv_filter_search_model_label?.text = getString(R.string.filter_search_choose_model_label)
        tv_filter_search_year_label?.text = getString(R.string.filter_search_choose_year_label)

        TextViewCompat.setTextAppearance(tv_filter_search_manufacturer_label, R.style.filter_search_ui_not_selected_textview)
        TextViewCompat.setTextAppearance(tv_filter_search_model_label, R.style.filter_search_ui_not_selected_textview)
        TextViewCompat.setTextAppearance(tv_filter_search_year_label, R.style.filter_search_ui_not_selected_textview)
    }

    override fun displayNewSearchView() {
        ll_filter_search_result?.hide()
        ll_filter_new_search?.show()
    }

    override fun displaySearchResultView() {
        tv_filter_search_result?.text = getString(R.string.filter_search_search_result_label, tv_filter_search_manufacturer_label.text, tv_filter_search_model_label.text, tv_filter_search_year_label.text)
        ll_filter_new_search?.hide()
        ll_filter_search_result?.show()
    }

    override fun displayManufacturerValue(value: String?) {
        value?.let { mValue ->
            tv_filter_search_manufacturer_label?.text = mValue
            TextViewCompat.setTextAppearance(tv_filter_search_manufacturer_label, R.style.filter_search_ui_selected_textview)
        }
    }

    override fun displayModelValue(value: String?) {
        value?.let { mValue ->
            tv_filter_search_model_label?.text = mValue
            TextViewCompat.setTextAppearance(tv_filter_search_model_label, R.style.filter_search_ui_selected_textview)
        }
    }

    override fun displayYearValue(value: String?) {
        value?.let { mValue ->
            tv_filter_search_year_label?.text = mValue
            TextViewCompat.setTextAppearance(tv_filter_search_year_label, R.style.filter_search_ui_selected_textview)
        }
    }

    override fun animateManufacturerView() {
        CommonUtils.playYoyo(rl_filter_search_manufacturer_btn)
    }

    override fun animateModelView() {
        CommonUtils.playYoyo(rl_filter_search_model_btn)
    }

    override fun animateYearView() {
        CommonUtils.playYoyo(rl_filter_search_year_btn)
    }

    override fun openFilterListFragment(filterParameter: AppConstants.FilterParameter) {
        val filterListFragment = FilterListFragment.newInstance()
        filterListFragment.filterParameter = filterParameter

        getBaseActivity()?.openNewFragment(fragment = filterListFragment,
                tag = FilterListFragment.TAG,
                title = FilterListFragment.TITLE,
                fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_SLIDE)
    }

    /**
     **********************************
     * FilterSearchMVPView implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * FilterSearchFragmentCallBack implementation
     * Start
     **********************************
     **/

    override fun onUpdateData() =
            presenter.onUpdateData()

    override fun onResetFiltersClicked() =
            presenter.onResetFilters()

    /**
     **********************************
     * FilterSearchFragmentCallBack implementation
     * End
     **********************************
     **/

    companion object {
        internal val TAG = "filter_search_ft"
        internal val TITLE = "Filter your search"

        fun newInstance(): FilterSearchFragment {
            return FilterSearchFragment()
        }
    }
}
