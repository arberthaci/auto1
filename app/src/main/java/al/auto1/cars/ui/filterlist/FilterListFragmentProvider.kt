package al.auto1.cars.ui.filterlist

import al.auto1.cars.ui.filterlist.view.FilterListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

@Module
internal abstract class FilterListFragmentProvider {

    @ContributesAndroidInjector(modules = [FilterListFragmentModule::class])
    internal abstract fun provideFilterListFragmentFactory(): FilterListFragment
}