package al.auto1.cars.ui.filterlist.interactor

import al.auto1.cars.data.network.model.WkdaResponse
import al.auto1.cars.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface FilterListMVPInteractor : MVPInteractor {

    fun updateManufacturerKeyInSharedPref(manufacturerKey: String?)
    fun updateManufacturerValueInSharedPref(manufacturerValue: String?)

    fun updateModelKeyInSharedPref(modelKey: String?)
    fun updateModelValueInSharedPref(modelValue: String?)

    fun updateYearKeyInSharedPref(yearKey: String?)
    fun updateYearValueInSharedPref(yearValue: String?)

    fun callRetrieveManufacturers(): Observable<WkdaResponse>
    fun callRetrieveMainTypes(filterParameters: Map<String, String>): Observable<WkdaResponse>
    fun callRetrieveBuiltDates(filterParameters: Map<String, String>): Observable<WkdaResponse>
}