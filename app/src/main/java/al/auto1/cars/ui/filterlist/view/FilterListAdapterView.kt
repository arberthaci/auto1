package al.auto1.cars.ui.filterlist.view

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface FilterListAdapterView {

    fun adapterNotifyDataSetChanged()
    fun adapterItemClicked(position: Int)
}