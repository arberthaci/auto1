package al.auto1.cars.ui.filtersearch.presenter

import al.auto1.cars.ui.base.presenter.BasePresenter
import al.auto1.cars.ui.filtersearch.interactor.FilterSearchMVPInteractor
import al.auto1.cars.ui.filtersearch.view.FilterSearchMVPView
import al.auto1.cars.utils.AppConstants
import al.auto1.cars.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class FilterSearchPresenter <V : FilterSearchMVPView, I : FilterSearchMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), FilterSearchMVPPresenter<V, I> {

    override fun onViewPrepared() =
        displayNewSearchView()

    override fun onChooseManufacturerClicked(): Unit? =
            getView()?.openFilterListFragment(filterParameter = AppConstants.FilterParameter.FILTER_MANUFACTURER)

    override fun onChooseModelClicked() {
        if (!interactor?.retrieveManufacturerKeyFromSharedPref().isNullOrEmpty())
            getView()?.openFilterListFragment(filterParameter = AppConstants.FilterParameter.FILTER_MODEL)
        else
            getView()?.animateManufacturerView()
    }

    override fun onChooseYearClicked() {
        if (!interactor?.retrieveManufacturerKeyFromSharedPref().isNullOrEmpty() && !interactor?.retrieveModelKeyFromSharedPref().isNullOrEmpty())
            getView()?.openFilterListFragment(filterParameter = AppConstants.FilterParameter.FILTER_YEAR)
        else if (!interactor?.retrieveManufacturerKeyFromSharedPref().isNullOrEmpty() && interactor?.retrieveModelKeyFromSharedPref().isNullOrEmpty())
            getView()?.animateModelView()
        else
            getView()?.let {
                it.animateManufacturerView()
                it.animateModelView()
            }
    }

    override fun onSearchClicked() {
        val isManufacturerValid = !interactor?.retrieveManufacturerKeyFromSharedPref().isNullOrEmpty()
        val isModelValid = !interactor?.retrieveModelKeyFromSharedPref().isNullOrEmpty()
        val isYearValid = !interactor?.retrieveYearKeyFromSharedPref().isNullOrEmpty()

        if (!isManufacturerValid)
            getView()?.animateManufacturerView()
        if (!isModelValid)
            getView()?.animateModelView()
        if (!isYearValid)
            getView()?.animateYearView()

        if (isManufacturerValid && isModelValid && isYearValid)
            getView()?.displaySearchResultView()
    }

    override fun onNewSearchClicked() =
            displayNewSearchView()

    override fun onUpdateData() =
        displayPreFilledDataIfExist()

    override fun onResetFilters() {
        interactor?.deleteAllDataInSharedPref()
        displayNewSearchView()
    }

    private fun displayNewSearchView() {
        getView()?.displayNewSearchView()
        displayPreFilledDataIfExist()
    }

    private fun displayPreFilledDataIfExist() {
        getView()?.resetDisplayedData()

        if (!interactor?.retrieveManufacturerKeyFromSharedPref().isNullOrEmpty())
            getView()?.displayManufacturerValue(value = interactor?.retrieveManufacturerValueFromSharedPref())

        if (!interactor?.retrieveModelKeyFromSharedPref().isNullOrEmpty())
            getView()?.displayModelValue(value = interactor?.retrieveModelValueFromSharedPref())

        if (!interactor?.retrieveYearKeyFromSharedPref().isNullOrEmpty())
            getView()?.displayYearValue(value = interactor?.retrieveYearValueFromSharedPref())
    }
}