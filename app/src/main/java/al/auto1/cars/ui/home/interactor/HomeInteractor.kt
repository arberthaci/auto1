package al.auto1.cars.ui.home.interactor

import al.auto1.cars.data.db.DbOperations
import al.auto1.cars.data.network.ApiHelper
import al.auto1.cars.data.prefs.PreferenceHelper
import al.auto1.cars.ui.base.interactor.BaseInteractor
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class HomeInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper, dbOperations = dbOperations), HomeMVPInteractor