package al.auto1.cars.ui.filterlist.view

import al.auto1.cars.R
import al.auto1.cars.ui.filterlist.presenter.FilterListAdapterPresenter
import al.auto1.cars.ui.filterlist.presenter.FilterListViewHolderPresenter
import al.auto1.cars.utils.rvadapterlisteners.OnFilterListActionListener
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_filter_list.view.*

/**
 * Created by Arbër Thaçi on 19-02-19.
 * Email: arberlthaci@gmail.com
 */

class FilterListAdapter (val presenter: FilterListAdapterPresenter)
    : RecyclerView.Adapter<FilterListAdapter.FilterListViewHolder>(), FilterListAdapterView {

    init {
        presenter.view = this
    }

    lateinit var onFilterListActionListener: OnFilterListActionListener

    override fun getItemCount() = this.presenter.getItemCount()

    override fun onBindViewHolder(holder: FilterListViewHolder, position: Int) = holder.let {
        holder.viewHolderPresenter.clearViewHolder(it)
        holder.viewHolderPresenter.onBindViewHolder(position, it)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FilterListViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_filter_list, parent, false))

    override fun adapterNotifyDataSetChanged() {
        notifyDataSetChanged()
    }

    override fun adapterItemClicked(position: Int) =
            onFilterListActionListener.onItemSelected(position = position)

    inner class FilterListViewHolder(view: View) : RecyclerView.ViewHolder(view), FilterListViewHolderView {

        val viewHolderPresenter = FilterListViewHolderPresenter(presenter)

        override fun setValue(value: String?) {
            value?.let { itemView.tv_filter_item_value.text = it }
                    ?: run { itemView.tv_filter_item_value.text = "" }
        }

        override fun setBackgroundColor() {
            if (adapterPosition %2 == 0)
                itemView.ll_single_filter_background.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.green_50))
            else
                itemView.ll_single_filter_background.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.blue_50))
        }

        override fun setOnClickListeners() {
            if (adapterPosition != RecyclerView.NO_POSITION)
                itemView.cv_single_filter_item.setOnClickListener { viewHolderPresenter.onClickListenerItem(adapterPosition) }
        }
    }
}