package al.auto1.cars.ui.base.interactor

import al.auto1.cars.data.db.DbOperations
import al.auto1.cars.data.network.ApiHelper
import al.auto1.cars.data.prefs.PreferenceHelper

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

open class BaseInteractor() : MVPInteractor {

    protected lateinit var preferenceHelper: PreferenceHelper
    protected lateinit var apiHelper: ApiHelper
    protected lateinit var dbOperations: DbOperations

    constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : this() {
        this.preferenceHelper = preferenceHelper
        this.apiHelper = apiHelper
        this.dbOperations = dbOperations
    }

    override fun retrieveManufacturerKeyFromSharedPref(): String? =
            this.preferenceHelper.getManufacturerKey()

    override fun retrieveModelKeyFromSharedPref(): String? =
            this.preferenceHelper.getModelKey()
}