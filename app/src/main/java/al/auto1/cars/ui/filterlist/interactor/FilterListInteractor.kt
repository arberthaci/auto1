package al.auto1.cars.ui.filterlist.interactor

import al.auto1.cars.data.db.DbOperations
import al.auto1.cars.data.network.ApiHelper
import al.auto1.cars.data.network.model.WkdaResponse
import al.auto1.cars.data.prefs.PreferenceHelper
import al.auto1.cars.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class FilterListInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper, dbOperations = dbOperations), FilterListMVPInteractor {

    override fun updateManufacturerKeyInSharedPref(manufacturerKey: String?) =
            preferenceHelper.setManufacturerKey(manufacturerKey = manufacturerKey)

    override fun updateManufacturerValueInSharedPref(manufacturerValue: String?) =
            preferenceHelper.setManufacturerValue(manufacturerValue = manufacturerValue)

    override fun updateModelKeyInSharedPref(modelKey: String?) =
            preferenceHelper.setModelKey(modelKey = modelKey)

    override fun updateModelValueInSharedPref(modelValue: String?) =
            preferenceHelper.setModelValue(modelValue = modelValue)

    override fun updateYearKeyInSharedPref(yearKey: String?) =
            preferenceHelper.setYearKey(yearKey = yearKey)

    override fun updateYearValueInSharedPref(yearValue: String?) =
            preferenceHelper.setYearValue(yearValue = yearValue)

    override fun callRetrieveManufacturers(): Observable<WkdaResponse> =
            apiHelper.provideManufacturerService().getManufacturers()

    override fun callRetrieveMainTypes(filterParameters: Map<String, String>): Observable<WkdaResponse> =
            apiHelper.provideMainTypesService().getMainTypes(filterParameters = filterParameters)

    override fun callRetrieveBuiltDates(filterParameters: Map<String, String>): Observable<WkdaResponse> =
            apiHelper.provideBuiltDatesService().getBuiltDates(filterParameters = filterParameters)
}