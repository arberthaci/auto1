package al.auto1.cars.data.prefs

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface PreferenceHelper {

    fun getManufacturerKey(): String?
    fun setManufacturerKey(manufacturerKey: String?)

    fun getManufacturerValue(): String?
    fun setManufacturerValue(manufacturerValue: String?)

    fun getModelKey(): String?
    fun setModelKey(modelKey: String?)

    fun getModelValue(): String?
    fun setModelValue(modelValue: String?)

    fun getYearKey(): String?
    fun setYearKey(yearKey: String?)

    fun getYearValue(): String?
    fun setYearValue(yearValue: String?)
}