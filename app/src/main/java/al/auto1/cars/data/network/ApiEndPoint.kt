package al.auto1.cars.data.network

import al.auto1.cars.BuildConfig

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

object ApiEndPoint {

    private const val API_POSTFIX = "/v1/car-types/"

    const val ENDPOINT_MANUFACTURER = BuildConfig.BASE_URL + API_POSTFIX + "manufacturer"
    const val ENDPOINT_MAIN_TYPES = BuildConfig.BASE_URL + API_POSTFIX + "main-types"
    const val ENDPOINT_BUILT_DATES = BuildConfig.BASE_URL + API_POSTFIX + "built-dates"
}