package al.auto1.cars.data.db

import io.realm.DynamicRealm
import io.realm.RealmMigration

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class DbMigration : RealmMigration {

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        //var oldVersion = oldVersion
        //val schema = realm.schema
    }


    override fun hashCode(): Int {
        return 37
    }

    override fun equals(other: Any?): Boolean {
        return other is DbMigration
    }
}