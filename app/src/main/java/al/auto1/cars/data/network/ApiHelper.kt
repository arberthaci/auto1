package al.auto1.cars.data.network

import al.auto1.cars.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class ApiHelper {

    private val API_HEADER_CONTENT_TYPE_NAME = "Content-Type"
    private val API_HEADER_ACCEPT_NAME = "Accept"
    private val API_HEADER_APPLICATION_JSON_VALUE = "application/json"

    private fun provideHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor { chain ->
                    val original = chain.request()
                    val originalHttpUrl = original.url()

                    val url = originalHttpUrl.newBuilder()
                            .addQueryParameter("wa_key", BuildConfig.AUTHORIZATION_KEY)
                            .build()

                    val requestBuilder = original.newBuilder()
                            .addHeader(API_HEADER_CONTENT_TYPE_NAME, API_HEADER_APPLICATION_JSON_VALUE)
                            .addHeader(API_HEADER_ACCEPT_NAME, API_HEADER_APPLICATION_JSON_VALUE)
                            .url(url)

                    val request = requestBuilder.build()
                    chain.proceed(request)
                }
                .build()
    }

    private fun provideRetrofit(): Retrofit {
        val gsonBuilder = GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .excludeFieldsWithoutExposeAnnotation()

        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(provideHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    fun provideManufacturerService(): ManufacturerApi =
            provideRetrofit().create(ManufacturerApi::class.java)

    fun provideMainTypesService(): MainTypesApi =
            provideRetrofit().create(MainTypesApi::class.java)

    fun provideBuiltDatesService(): BuiltDatesApi =
            provideRetrofit().create(BuiltDatesApi::class.java)
}