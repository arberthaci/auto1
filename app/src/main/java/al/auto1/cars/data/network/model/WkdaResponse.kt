package al.auto1.cars.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

data class WkdaResponse(@Expose
                        @SerializedName("page")
                        var page: Int? = 0,

                        @Expose
                        @SerializedName("pageSize")
                        var pageSize: Int? = null,

                        @Expose
                        @SerializedName("totalPageCount")
                        var totalPageCount: Int? = null,

                        @Expose
                        @SerializedName("wkda")
                        var wkda: LinkedHashMap<String, String>? = null)