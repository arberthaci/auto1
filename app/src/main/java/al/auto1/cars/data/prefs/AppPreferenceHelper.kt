package al.auto1.cars.data.prefs

import al.auto1.cars.di.PreferenceInfo
import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

class AppPreferenceHelper @Inject constructor(context: Context, @PreferenceInfo private val prefFileName: String) : PreferenceHelper {

    companion object {
        private const val PREF_KEY_MANUFACTURER_KEY = "PREF_KEY_MANUFACTURER_KEY"
        private const val PREF_KEY_MANUFACTURER_VALUE = "PREF_KEY_MANUFACTURER_VALUE"
        private const val PREF_KEY_MODEL_KEY = "PREF_KEY_MODEL_KEY"
        private const val PREF_KEY_MODEL_VALUE = "PREF_KEY_MODEL_VALUE"
        private const val PREF_KEY_YEAR_KEY = "PREF_KEY_YEAR_KEY"
        private const val PREF_KEY_YEAR_VALUE = "PREF_KEY_YEAR_VALUE"
    }

    private val mPrefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    override fun getManufacturerKey(): String? =
            mPrefs.getString(PREF_KEY_MANUFACTURER_KEY, null)

    override fun setManufacturerKey(manufacturerKey: String?) {
        mPrefs.edit {
            if (manufacturerKey == null) remove(PREF_KEY_MANUFACTURER_KEY)
            else putString(PREF_KEY_MANUFACTURER_KEY, manufacturerKey)
        }
    }

    override fun getManufacturerValue(): String? =
            mPrefs.getString(PREF_KEY_MANUFACTURER_VALUE, null)

    override fun setManufacturerValue(manufacturerValue: String?) {
        mPrefs.edit {
            if (manufacturerValue == null) remove(PREF_KEY_MANUFACTURER_VALUE)
            else putString(PREF_KEY_MANUFACTURER_VALUE, manufacturerValue)
        }
    }

    override fun getModelKey(): String? =
            mPrefs.getString(PREF_KEY_MODEL_KEY, null)

    override fun setModelKey(modelKey: String?) {
        mPrefs.edit {
            if (modelKey == null) remove(PREF_KEY_MODEL_KEY)
            else putString(PREF_KEY_MODEL_KEY, modelKey)
        }
    }

    override fun getModelValue(): String? =
            mPrefs.getString(PREF_KEY_MODEL_VALUE, null)

    override fun setModelValue(modelValue: String?) {
        mPrefs.edit {
            if (modelValue == null) remove(PREF_KEY_MODEL_VALUE)
            else putString(PREF_KEY_MODEL_VALUE, modelValue)
        }
    }

    override fun getYearKey(): String? =
            mPrefs.getString(PREF_KEY_YEAR_KEY, null)

    override fun setYearKey(yearKey: String?) {
        mPrefs.edit {
            if (yearKey == null) remove(PREF_KEY_YEAR_KEY)
            else putString(PREF_KEY_YEAR_KEY, yearKey)
        }
    }

    override fun getYearValue(): String? =
            mPrefs.getString(PREF_KEY_YEAR_VALUE, null)

    override fun setYearValue(yearValue: String?) {
        mPrefs.edit {
            if (yearValue == null) remove(PREF_KEY_YEAR_VALUE)
            else putString(PREF_KEY_YEAR_VALUE, yearValue)
        }
    }
}