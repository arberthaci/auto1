package al.auto1.cars.data.network

import al.auto1.cars.data.network.model.WkdaResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

interface MainTypesApi {

    @GET(ApiEndPoint.ENDPOINT_MAIN_TYPES)
    fun getMainTypes(@QueryMap filterParameters: Map<String, String>?): Observable<WkdaResponse>
}