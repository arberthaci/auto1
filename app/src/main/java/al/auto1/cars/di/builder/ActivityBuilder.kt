package al.auto1.cars.di.builder

import al.auto1.cars.ui.filterlist.FilterListFragmentProvider
import al.auto1.cars.ui.filtersearch.FilterSearchFragmentProvider
import al.auto1.cars.ui.home.HomeActivityModule
import al.auto1.cars.ui.home.view.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(HomeActivityModule::class), (FilterSearchFragmentProvider::class), (FilterListFragmentProvider::class)])

    abstract fun bindHomeActivity(): HomeActivity
}