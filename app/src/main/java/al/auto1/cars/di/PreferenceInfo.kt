package al.auto1.cars.di

import javax.inject.Qualifier

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

@Qualifier
@Retention annotation class PreferenceInfo