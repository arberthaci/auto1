package al.auto1.cars.di.module

import al.auto1.cars.data.db.DbMigration
import al.auto1.cars.data.db.DbOperations
import al.auto1.cars.data.network.ApiHelper
import al.auto1.cars.data.prefs.AppPreferenceHelper
import al.auto1.cars.data.prefs.PreferenceHelper
import al.auto1.cars.di.PreferenceInfo
import al.auto1.cars.utils.AppConstants
import al.auto1.cars.utils.rx.SchedulerProvider
import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton

/**
 * Created by Arbër Thaçi on 19-02-18.
 * Email: arberlthaci@gmail.com
 */

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideRealmDatabase(context: Context): Realm {
        Realm.init(context)
        return Realm.getInstance(RealmConfiguration.Builder()
                .name("realm.auto1.cars")
                .schemaVersion(1)
                .migration(DbMigration())
                .build())
    }

    @Provides
    @PreferenceInfo
    internal fun providePrefFileName(): String = AppConstants.SHARED_PREFS_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    @Singleton
    internal fun provideApiHelper(): ApiHelper = ApiHelper()

    @Provides
    @Singleton
    internal fun provideDbOperations(): DbOperations = DbOperations()

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()
}