package al.auto1.cars

import al.auto1.cars.ui.filtersearch.interactor.FilterSearchMVPInteractor
import al.auto1.cars.ui.filtersearch.presenter.FilterSearchPresenter
import al.auto1.cars.ui.filtersearch.view.FilterSearchMVPView
import al.auto1.cars.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

/**
 * Created by Arbër Thaçi on 19-02-20.
 * Email: arberlthaci@gmail.com
 */

class FilterSearchPresenterTest {

    /**
     * In these tests
     * we're covering only the
     * `onViewPrepared()` cases
     **/

    private val view = mock(FilterSearchMVPView::class.java)
    private val interactor = mock(FilterSearchMVPInteractor::class.java)

    private lateinit var presenter: FilterSearchPresenter<FilterSearchMVPView, FilterSearchMVPInteractor>

    @Before
    fun setUp() {
        val compositeDisposable = CompositeDisposable()
        presenter = FilterSearchPresenter(interactor, SchedulerProvider(), compositeDisposable)
        presenter.onAttach(view)
    }

    @Test
    fun onViewPrepared_noKeyIsValid_noValueIsDisplayed() {
        val manufacturerKey = null
        val manufacturerValue = null
        val modelKey = null
        val modelValue = null
        val yearKey = null
        val yearValue = null

        `when`(interactor.retrieveManufacturerKeyFromSharedPref()).thenReturn(manufacturerKey)
        `when`(interactor.retrieveManufacturerValueFromSharedPref()).thenReturn(manufacturerValue)
        `when`(interactor.retrieveModelKeyFromSharedPref()).thenReturn(modelKey)
        `when`(interactor.retrieveModelValueFromSharedPref()).thenReturn(modelValue)
        `when`(interactor.retrieveYearKeyFromSharedPref()).thenReturn(yearKey)
        `when`(interactor.retrieveYearValueFromSharedPref()).thenReturn(yearValue)

        presenter.onViewPrepared()
        verify(view, times(1)).displayNewSearchView()
        verify(view, times(1)).resetDisplayedData()
        verify(view, times(0)).displayManufacturerValue(manufacturerValue)
        verify(view, times(0)).displayModelValue(modelValue)
        verify(view, times(0)).displayYearValue(yearValue)
    }

    @Test
    fun onViewPrepared_onlyManufacturerKeyIsValid_onlyManufacturerValueIsDisplayed() {
        val manufacturerKey = "key1"
        val manufacturerValue = "value1"
        val modelKey = null
        val modelValue = null
        val yearKey = null
        val yearValue = null

        `when`(interactor.retrieveManufacturerKeyFromSharedPref()).thenReturn(manufacturerKey)
        `when`(interactor.retrieveManufacturerValueFromSharedPref()).thenReturn(manufacturerValue)
        `when`(interactor.retrieveModelKeyFromSharedPref()).thenReturn(modelKey)
        `when`(interactor.retrieveModelValueFromSharedPref()).thenReturn(modelValue)
        `when`(interactor.retrieveYearKeyFromSharedPref()).thenReturn(yearKey)
        `when`(interactor.retrieveYearValueFromSharedPref()).thenReturn(yearValue)

        presenter.onViewPrepared()
        verify(view, times(1)).displayNewSearchView()
        verify(view, times(1)).resetDisplayedData()
        verify(view, times(1)).displayManufacturerValue(manufacturerValue)
        verify(view, times(0)).displayModelValue(modelValue)
        verify(view, times(0)).displayYearValue(yearValue)
    }

    @Test
    fun onViewPrepared_manufacturerAndModelKeysAreValid_manufacturerAndModelValuesAreDisplayed() {
        val manufacturerKey = "key1"
        val manufacturerValue = "value1"
        val modelKey = "key2"
        val modelValue = "value2"
        val yearKey = null
        val yearValue = null

        `when`(interactor.retrieveManufacturerKeyFromSharedPref()).thenReturn(manufacturerKey)
        `when`(interactor.retrieveManufacturerValueFromSharedPref()).thenReturn(manufacturerValue)
        `when`(interactor.retrieveModelKeyFromSharedPref()).thenReturn(modelKey)
        `when`(interactor.retrieveModelValueFromSharedPref()).thenReturn(modelValue)
        `when`(interactor.retrieveYearKeyFromSharedPref()).thenReturn(yearKey)
        `when`(interactor.retrieveYearValueFromSharedPref()).thenReturn(yearValue)

        presenter.onViewPrepared()
        verify(view, times(1)).displayNewSearchView()
        verify(view, times(1)).resetDisplayedData()
        verify(view, times(1)).displayManufacturerValue(manufacturerValue)
        verify(view, times(1)).displayModelValue(modelValue)
        verify(view, times(0)).displayYearValue(yearValue)
    }

    @Test
    fun onViewPrepared_manufacturerAndYearKeysAreValid_manufacturerAndYearValuesAreDisplayed() {
        val manufacturerKey = "key1"
        val manufacturerValue = "value1"
        val modelKey = null
        val modelValue = null
        val yearKey = "key3"
        val yearValue = "model3"

        `when`(interactor.retrieveManufacturerKeyFromSharedPref()).thenReturn(manufacturerKey)
        `when`(interactor.retrieveManufacturerValueFromSharedPref()).thenReturn(manufacturerValue)
        `when`(interactor.retrieveModelKeyFromSharedPref()).thenReturn(modelKey)
        `when`(interactor.retrieveModelValueFromSharedPref()).thenReturn(modelValue)
        `when`(interactor.retrieveYearKeyFromSharedPref()).thenReturn(yearKey)
        `when`(interactor.retrieveYearValueFromSharedPref()).thenReturn(yearValue)

        presenter.onViewPrepared()
        verify(view, times(1)).displayNewSearchView()
        verify(view, times(1)).resetDisplayedData()
        verify(view, times(1)).displayManufacturerValue(manufacturerValue)
        verify(view, times(0)).displayModelValue(modelValue)
        verify(view, times(1)).displayYearValue(yearValue)
    }

    @Test
    fun onViewPrepared_modelAndYearKeysAreValid_modelAndYearValuesAreDisplayed() {
        val manufacturerKey = null
        val manufacturerValue = null
        val modelKey = "key2"
        val modelValue = "model2"
        val yearKey = "key3"
        val yearValue = "model3"

        `when`(interactor.retrieveManufacturerKeyFromSharedPref()).thenReturn(manufacturerKey)
        `when`(interactor.retrieveManufacturerValueFromSharedPref()).thenReturn(manufacturerValue)
        `when`(interactor.retrieveModelKeyFromSharedPref()).thenReturn(modelKey)
        `when`(interactor.retrieveModelValueFromSharedPref()).thenReturn(modelValue)
        `when`(interactor.retrieveYearKeyFromSharedPref()).thenReturn(yearKey)
        `when`(interactor.retrieveYearValueFromSharedPref()).thenReturn(yearValue)

        presenter.onViewPrepared()
        verify(view, times(1)).displayNewSearchView()
        verify(view, times(1)).resetDisplayedData()
        verify(view, times(0)).displayManufacturerValue(manufacturerValue)
        verify(view, times(1)).displayModelValue(modelValue)
        verify(view, times(1)).displayYearValue(yearValue)
    }

    @Test
    fun onViewPrepared_allKeysAreValid_allValuesAreDisplayed() {
        val manufacturerKey = "key1"
        val manufacturerValue = "model1"
        val modelKey = "key2"
        val modelValue = "model2"
        val yearKey = "key3"
        val yearValue = "model3"

        `when`(interactor.retrieveManufacturerKeyFromSharedPref()).thenReturn(manufacturerKey)
        `when`(interactor.retrieveManufacturerValueFromSharedPref()).thenReturn(manufacturerValue)
        `when`(interactor.retrieveModelKeyFromSharedPref()).thenReturn(modelKey)
        `when`(interactor.retrieveModelValueFromSharedPref()).thenReturn(modelValue)
        `when`(interactor.retrieveYearKeyFromSharedPref()).thenReturn(yearKey)
        `when`(interactor.retrieveYearValueFromSharedPref()).thenReturn(yearValue)

        presenter.onViewPrepared()
        verify(view, times(1)).displayNewSearchView()
        verify(view, times(1)).resetDisplayedData()
        verify(view, times(1)).displayManufacturerValue(manufacturerValue)
        verify(view, times(1)).displayModelValue(modelValue)
        verify(view, times(1)).displayYearValue(yearValue)
    }
}