package al.auto1.cars

import al.auto1.cars.data.network.ApiHelper
import al.auto1.cars.data.network.model.WkdaResponse
import java.util.HashMap

import com.google.gson.Gson
import io.reactivex.observers.TestObserver
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer

import org.junit.Before
import org.junit.Test
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals

/**
 * Created by Arbër Thaçi on 19-02-20.
 * Email: arberlthaci@gmail.com
 */

class BuiltDatesApiTest {

    private lateinit var gson: Gson
    private lateinit var mockWebServer: MockWebServer
    private lateinit var testObserver: TestObserver<WkdaResponse>

    private val goodResponseJson = "{\"page\":0,\"wkda\":{\"2007\":\"2007\",\"2008\":\"2008\",\"2009\":\"2009\",\"2010\":\"2010\",\"2011\":\"2011\",\"2012\":\"2012\"}}"
    private val badResponseJson = "{\"page\":\"false\",\"wkda\":{\"2007\":\"2008\",\"2008\":\"2008\"}}"

    @Before
    fun setUp() {
        gson = Gson()
        mockWebServer = MockWebServer()
        testObserver = TestObserver()
    }

    /**
     * In this use case,
     * parsing of the response
     * SUCCESSFULLY passes the test
     **/
    @Test
    fun serverCallWithSuccessfulParsing() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson)
        mockWebServer.enqueue(mockResponse)

        val filters: HashMap<String, String> = hashMapOf()
        filters["manufacturer"] = "057"
        filters["main-type"] = "DBS"
        ApiHelper().provideBuiltDatesService().getBuiltDates(filterParameters = filters).subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        assertEquals(goodResponseJson, gson.toJson(testObserver.values()[0]))
    }

    /**
     * In this use case,
     * parsing of the invalid response
     * SUCCESSFULLY passes the assertNotEquals test
     **/
    @Test
    fun serverCallWithErrorParsing() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(badResponseJson)
        mockWebServer.enqueue(mockResponse)

        val filters: HashMap<String, String> = hashMapOf()
        filters["manufacturer"] = "057"
        filters["main-type"] = "DBS"
        ApiHelper().provideBuiltDatesService().getBuiltDates(filterParameters = filters).subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        assertNotEquals(badResponseJson, gson.toJson(testObserver.values()[0]))
    }

    /**
     * In this use case,
     * handling the 200 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallForTestingEndpoint() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson)
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["manufacturer"] = "057"
        filters["main-type"] = "DBS"
        ApiHelper().provideBuiltDatesService().getBuiltDates(filterParameters = filters).subscribe(testObserver)

        assertEquals("HTTP/1.1 200 OK", mockResponse.status)

        mockWebServer.shutdown()
    }

    /**
     * In this use case,
     * handling the 404 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallWith404ResponseCode() {
        val mockResponse = MockResponse()
                .setResponseCode(404)
                .setBody("{}")
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["manufacturer"] = "057"
        filters["main-type"] = "DBS"
        ApiHelper().provideBuiltDatesService().getBuiltDates(filterParameters = filters).subscribe(testObserver)

        assertEquals("HTTP/1.1 404 Client Error", mockResponse.status)

        mockWebServer.shutdown()
    }

    /**
     * In this use case,
     * handling the 500 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallWith500ResponseCode() {
        val mockResponse = MockResponse()
                .setResponseCode(500)
                .setBody("{}")
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["manufacturer"] = "057"
        filters["main-type"] = "DBS"
        ApiHelper().provideBuiltDatesService().getBuiltDates(filterParameters = filters).subscribe(testObserver)

        assertEquals("HTTP/1.1 500 Server Error", mockResponse.status)

        mockWebServer.shutdown()
    }
}