package al.auto1.cars

import al.auto1.cars.data.network.model.WkdaResponse
import al.auto1.cars.ui.filterlist.interactor.FilterListMVPInteractor
import al.auto1.cars.ui.filterlist.presenter.FilterListPresenter
import al.auto1.cars.ui.filterlist.view.FilterListMVPView
import al.auto1.cars.utils.AppConstants
import al.auto1.cars.utils.customviews.ErrorViewState
import al.auto1.cars.utils.rx.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*

/**
 * Created by Arbër Thaçi on 19-02-20.
 * Email: arberlthaci@gmail.com
 */

class FilterListPresenterTest {

    /**
     * In these tests
     * we're covering only the
     * `onViewPrepared()` cases
     **/

    @Rule
    @JvmField var testSchedulerRule = RxImmediateSchedulerRule()

    private val view = mock(FilterListMVPView::class.java)
    private val interactor = mock(FilterListMVPInteractor::class.java)

    private lateinit var presenter: FilterListPresenter<FilterListMVPView, FilterListMVPInteractor>

    @Before
    fun setUp() {
        presenter = FilterListPresenter(interactor, SchedulerProvider(), CompositeDisposable())
        presenter.onAttach(view)
    }

    @Test
    fun onViewPrepared_manufacturerNoInternet_noInternetDisplayed() {
        `when`(interactor.callRetrieveManufacturers()).thenReturn(Observable.just(WkdaResponse()))
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(false))

        presenter.onViewPrepared(AppConstants.FilterParameter.FILTER_MANUFACTURER)

        verify(view, times(1)).updateSearchPlaceholder("manufacturer")
        verify(view, times(1)).prepareLoading()
        verify(view, times(1)).isNetworkConnected()
        verify(view, times(0)).displayFilterList(null)
        verify(view, times(1)).showErrorView(ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(ErrorViewState.NO_RESPONSE)
        verify(view, times(0)).showErrorView(ErrorViewState.NO_RESULTS)
    }

    @Test
    fun onViewPrepared_manufacturerEmptyResponse_noResultsDisplayed() {
        val wkdaResponse = WkdaResponse(wkda = LinkedHashMap())
        `when`(interactor.callRetrieveManufacturers()).thenReturn(Observable.just(wkdaResponse))
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))

        presenter.onViewPrepared(AppConstants.FilterParameter.FILTER_MANUFACTURER)

        verify(view, times(1)).updateSearchPlaceholder("manufacturer")
        verify(view, times(1)).prepareLoading()
        verify(view, times(1)).isNetworkConnected()
        verify(view, times(0)).displayFilterList(wkdaResponse.wkda?.values?.toList())
        verify(view, times(0)).showErrorView(ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(ErrorViewState.NO_RESPONSE)
        verify(view, times(1)).showErrorView(ErrorViewState.NO_RESULTS)
    }

    @Test
    fun onViewPrepared_manufacturerException_noResponseDisplayed() {
        `when`(interactor.callRetrieveManufacturers()).thenReturn(Observable.error(Exception()))
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))

        presenter.onViewPrepared(AppConstants.FilterParameter.FILTER_MANUFACTURER)

        verify(view, times(1)).updateSearchPlaceholder("manufacturer")
        verify(view, times(1)).prepareLoading()
        verify(view, times(1)).isNetworkConnected()
        verify(view, times(0)).displayFilterList(null)
        verify(view, times(0)).showErrorView(ErrorViewState.NO_CONNECTION)
        verify(view, times(1)).showErrorView(ErrorViewState.NO_RESPONSE)
        verify(view, times(0)).showErrorView(ErrorViewState.NO_RESULTS)
    }

    @Test
    fun onViewPrepared_manufacturerValidResponse_displayFilterListDisplayed() {
        val linkedHashMap = LinkedHashMap<String, String>()
        linkedHashMap["key1"] = "value1"
        val wkdaResponse = WkdaResponse(wkda = linkedHashMap)
        `when`(interactor.callRetrieveManufacturers()).thenReturn(Observable.just(wkdaResponse))
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))

        presenter.onViewPrepared(AppConstants.FilterParameter.FILTER_MANUFACTURER)

        verify(view, times(1)).updateSearchPlaceholder("manufacturer")
        verify(view, times(1)).prepareLoading()
        verify(view, times(1)).isNetworkConnected()
        verify(view, times(1)).displayFilterList(wkdaResponse.wkda?.values?.toList())
        verify(view, times(0)).showErrorView(ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(ErrorViewState.NO_RESPONSE)
        verify(view, times(0)).showErrorView(ErrorViewState.NO_RESULTS)
    }
}