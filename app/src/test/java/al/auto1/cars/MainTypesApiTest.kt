package al.auto1.cars

import al.auto1.cars.data.network.ApiHelper
import al.auto1.cars.data.network.model.WkdaResponse
import java.util.HashMap

import com.google.gson.Gson
import io.reactivex.observers.TestObserver
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer

import org.junit.Before
import org.junit.Test
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals

/**
 * Created by Arbër Thaçi on 19-02-20.
 * Email: arberlthaci@gmail.com
 */

class MainTypesApiTest {

    private lateinit var gson: Gson
    private lateinit var mockWebServer: MockWebServer
    private lateinit var testObserver: TestObserver<WkdaResponse>

    private val goodResponseJson = "{\"page\":0,\"pageSize\":5,\"totalPageCount\":9,\"wkda\":{\"107 Coupe/Roadster\":\"107 Coupe/Roadster\",\"123\":\"123\",\"190/190 E\":\"190/190 E\",\"207 D - 210 D\":\"207 D - 210 D\",\"A-Klasse\":\"A-Klasse\"}}"
    private val badResponseJson = "{\"page\":1,\"pageSize\":0,\"totalPageCount\":\"true\",\"wkda\":{\"107 Coupe/Roadster\":\"123\",\"057\":\"90/190 E\"}}"

    @Before
    fun setUp() {
        gson = Gson()
        mockWebServer = MockWebServer()
        testObserver = TestObserver()
    }

    /**
     * In this use case,
     * parsing of the response
     * SUCCESSFULLY passes the test
     **/
    @Test
    fun serverCallWithSuccessfulParsing() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson)
        mockWebServer.enqueue(mockResponse)

        val filters: HashMap<String, String> = hashMapOf()
        filters["page"] = "0"
        filters["pageSize"] = "5"
        filters["manufacturer"] = "570"
        ApiHelper().provideMainTypesService().getMainTypes(filterParameters = filters).subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        assertEquals(goodResponseJson, gson.toJson(testObserver.values()[0]))
    }

    /**
     * In this use case,
     * parsing of the invalid response
     * SUCCESSFULLY passes the assertNotEquals test
     **/
    @Test
    fun serverCallWithErrorParsing() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(badResponseJson)
        mockWebServer.enqueue(mockResponse)

        val filters: HashMap<String, String> = hashMapOf()
        filters["page"] = "0"
        filters["pageSize"] = "5"
        filters["manufacturer"] = "570"
        ApiHelper().provideMainTypesService().getMainTypes(filterParameters = filters).subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        assertNotEquals(badResponseJson, gson.toJson(testObserver.values()[0]))
    }

    /**
     * In this use case,
     * handling the 200 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallForTestingEndpoint() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson)
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["page"] = "0"
        filters["pageSize"] = "5"
        filters["manufacturer"] = "570"
        ApiHelper().provideMainTypesService().getMainTypes(filterParameters = filters).subscribe(testObserver)

        assertEquals("HTTP/1.1 200 OK", mockResponse.status)

        mockWebServer.shutdown()
    }

    /**
     * In this use case,
     * handling the 404 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallWith404ResponseCode() {
        val mockResponse = MockResponse()
                .setResponseCode(404)
                .setBody("{}")
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["page"] = "0"
        filters["pageSize"] = "5"
        filters["manufacturer"] = "570"
        ApiHelper().provideMainTypesService().getMainTypes(filterParameters = filters).subscribe(testObserver)

        assertEquals("HTTP/1.1 404 Client Error", mockResponse.status)

        mockWebServer.shutdown()
    }

    /**
     * In this use case,
     * handling the 500 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallWith500ResponseCode() {
        val mockResponse = MockResponse()
                .setResponseCode(500)
                .setBody("{}")
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["page"] = "0"
        filters["pageSize"] = "5"
        filters["manufacturer"] = "570"
        ApiHelper().provideMainTypesService().getMainTypes(filterParameters = filters).subscribe(testObserver)

        assertEquals("HTTP/1.1 500 Server Error", mockResponse.status)

        mockWebServer.shutdown()
    }
}